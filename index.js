const config = require('./config');
const utilities = require('./utilities');
const { Poster } = require('./poster');

const main = () => {
    // Check if we have any proxies.
    let proxiesExist = utilities.proxiesExist(config.proxies);
    if (proxiesExist) {
        for (const [i, proxy] of config.proxies.entries()) {
            new Poster(i, proxy);
        }
    } else {
        new Poster("localhost");
    }
};

main();
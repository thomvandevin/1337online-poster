const fetch = require('node-fetch');
const FormData = require('form-data');
const ProxyAgent = require('proxy-agent');

const url = 'https://1337online.com/manager.php';

async function submit(proxy, name) {
    let form = new FormData();
    form.append('action', 'new');
    form.append('data', name);

    let options = {
        method: 'post',
        body: form,
        headers: {
            'Referer': 'https://1337online.com/'
        }
    };

    if (proxy) {
        options.agent = new ProxyAgent(proxy);
    }

    let call = await fetch(url, options);

    console.log(`SERVER response status: ${call.statusText}`);

    let response = await call.text();
    return response;
}

async function getFastestResponseTime(proxy, tries) {
    let options = {
        method: 'get'
    };

    if (proxy) {
        options.agent = new ProxyAgent(proxy);
    }

    let fastestResponse = -1;
    for (let i = 0; i < tries; i++) {
        let currentTime = new Date();
        let call = await fetch(url, options);

        console.log(`SERVER response status: ${call.statusText}`);

        if (call.statusText = 'OK') {
            let time = new Date() - currentTime;
            if (time < fastestResponse || fastestResponse < 0) {
                fastestResponse = time;
            }
        } else {
            tries++;
        }
    }
    return fastestResponse;
}

module.exports = {
    submit,
    getFastestResponseTime
}
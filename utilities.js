exports.proxiesExist = proxies => {
    return Array.isArray(proxies) && proxies.length;
}

exports.formatProxy = proxy => {
    if (proxy && ["localhost", ""].indexOf(proxy) < 0) {
        proxy = proxy.replace(" ", "_");
        const proxySplit = proxy.split(":");
        if (proxySplit.length > 3)
            return (
                "http://" +
                proxySplit[2] +
                ":" +
                proxySplit[3] +
                "@" +
                proxySplit[0] +
                ":" +
                proxySplit[1]
            );
        else return "http://" + proxySplit[0] + ":" + proxySplit[1];
    } else return undefined;
}

exports.formatPostTime = time => {
    let timeSplit = time.split(":");
    let formattedTime = new Date();
    formattedTime.setHours(timeSplit[0], timeSplit[1], timeSplit[2], timeSplit[3]);
    if ((formattedTime - new Date()) <= 0) {
        formattedTime.setDate(formattedTime.getDate() + 1);
    }
    return formattedTime;
}
# 1337 online poster
A small program to post your name to the 1337online.com website at the time you specify. 
The program awaits its time until 13:37:00, then sends a post request containing your name to the server to submit your entry. The program allows to be ran from a service, making it easier/cleaner. 

## Getting Started

### Prerequisites
* NodeJS and Node Package Manager are required. 
* For running as a service `qckwinsvc` is required on a global install. 

### Configuration
* Rename or copy the `config.example.json` to `config.json`.
* Setup the config file accordingly.
* Program can run with or without proxies, to use proxies and local: add `'localhost'` to the proxy list.

## Running

Simply run `node .` or `npm start` to start the program. 

### Installing as service

Install the program as a service by invoking the command:
> npm run service

to uninstall: 
> npm run unservice

The service will be available under the name: `Leet-Online-Poster` 

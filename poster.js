const config = require('./config');
const utilities = require('./utilities');
const leet = require('./1337');

class Poster {

    constructor(id, proxy) {
        this.id = id;
        this.proxy = utilities.formatProxy(proxy);
        console.log(`starting 1337-poster with proxy: ${this.proxy}`);
        this.initialize();
    }

    async initialize() {
        this.prepared = false;
        this.responseTime = 0;
        this.postTime = utilities.formatPostTime(config.postTime);
        //await this.updateResponseTime();
        this.start();
    }

    async updateResponseTime() {
        this.responseTime = await leet.getFastestResponseTime(this.proxy, config.responseTries);
        console.log(`server minimal response time: ${this.responseTime}`);
    }

    async start() {
        // Calculate the corrected time including the latency to the server
        let currentTime = new Date();
        // Transform responseTime into a postTime by cutting it in half
        let timeDifference = (this.postTime - new Date()) - (this.responseTime / 2) - config.compensationTime;

        // Update the server response time 5 minutes before the time.
        let prepare = timeDifference < 300000;
        if (prepare && this.prepared == false) {
            await this.updateResponseTime();
            this.prepared = true;
        }

        let post = timeDifference <= 0;
        let waitTime = timeDifference < config.timeout ? timeDifference : config.timeout;
        console.log(`current time: ${currentTime.toLocaleTimeString()}, time to post: ${post}, waiting time: ${timeDifference}, timeout: ${waitTime}`);
        if (!post) {
            await setTimeout(() => {
                this.start()
            }, waitTime);
            return;
        }

        console.log(`posting at time: ${new Date()}`)
        let serverResponse = await leet.submit(this.proxy, config.name);
        console.log(`server response: ${serverResponse}`);

        await this.initialize();
    }
}

module.exports = {
    Poster
}